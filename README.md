# weather-app

A Simple Weather App using Android Jetpack Architecture Component

## Project Structure

### data
Folder for data source. Connect to OpenWeatherMap API, creating entities, manage repositories, etc.

### di
Used for Dependency Injection related stuff

### features
Folder for each feature. Each feature must be contained in a separate folder

### utils
Contains reusable classes or extensions that can be used in another folder


### Minimum Android Version
* Android Lollipop 5.0 (API 21) or above