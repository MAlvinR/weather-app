package me.malvinr.weatherapp.features.domain

import kotlinx.coroutines.runBlocking
import me.malvinr.weatherapp.data.entity.DailyForecastResponse
import me.malvinr.weatherapp.data.entity.ForecastResponse
import me.malvinr.weatherapp.data.repository.WeatherRepository
import me.malvinr.weatherapp.features.ForecastUseCase
import me.malvinr.weatherapp.util.state.ResultState
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import retrofit2.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import okhttp3.MediaType.Companion.toMediaType

class ForecastUseCaseTest {

    private val repository = mock(WeatherRepository::class.java)
    private lateinit var useCase: ForecastUseCase

    private val dailyForecasts = listOf(
        DailyForecastResponse.Forecast(
            12345,
            DailyForecastResponse.Temperature(
                1.0,
                2.0
            ),
            listOf(
                DailyForecastResponse.Weather(
                    12345
                )
            )
        )
    )

    private val hourlyForecasts = listOf(
        ForecastResponse.Forecast(
            12345,
            listOf(
                ForecastResponse.Weather(
                    12345
                )
            ),
            ForecastResponse.Main(
                1.0,
                1.0,
                2.0
            )
        )
    )

    @Before
    fun setUp() {
        useCase = ForecastUseCase(repository)
    }

    @Test
    fun `should get daily forecast success`() {
        val actual = ResultState.Success(DailyForecastResponse(dailyForecasts))
        val result = runBlocking {
            `when`(repository.getDailyForecast())
                .thenReturn(Response.success(DailyForecastResponse(dailyForecasts)))
            useCase.getDailyForecast()
        }
        assert(result == actual)
    }

    @Test
    fun `should get daily forecast error`() {
        val actual = ResultState.Error("")
        val result = runBlocking {
            `when`(repository.getDailyForecast())
                .thenReturn(
                    Response.error(
                        401,
                        "".toResponseBody("application/json".toMediaType())
                    )
                )
            useCase.getDailyForecast()
        }
        assert(result.javaClass == actual.javaClass)
    }

    @Test
    fun `should get hourly forecast success`() {
        val actual = ResultState.Success(ForecastResponse(hourlyForecasts))
        val result = runBlocking {
            `when`(repository.getCurrentForecast())
                .thenReturn(Response.success(ForecastResponse(hourlyForecasts)))
            useCase.getCurrentForecast()
        }
        assert(result == actual)
    }

    @Test
    fun `should get hourly forecast error`() {
        val actual = ResultState.Error("")
        val result = runBlocking {
            `when`(repository.getCurrentForecast())
                .thenReturn(
                    Response.error(
                        401,
                        "".toResponseBody("application/json".toMediaType())
                    )
                )
            useCase.getCurrentForecast()
        }
        assert(result.javaClass == actual.javaClass)
    }
}