package me.malvinr.weatherapp.features.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import me.malvinr.weatherapp.data.entity.DailyForecastResponse
import me.malvinr.weatherapp.data.entity.ForecastResponse
import me.malvinr.weatherapp.features.ForecastListViewModel
import me.malvinr.weatherapp.features.ForecastUseCase
import me.malvinr.weatherapp.util.state.ResultState
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class ForecastListViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var dailyForecastResult: Observer<List<DailyForecastResponse.Forecast>>
    @Mock
    lateinit var dailyForecastError: Observer<String>

    @Mock
    lateinit var hourlyForecastResult: Observer<List<ForecastResponse.Forecast>>
    @Mock
    lateinit var hourlyForecastError: Observer<String>

    @Captor
    lateinit var argResultCaptorDailyForecast: ArgumentCaptor<List<DailyForecastResponse.Forecast>>
    @Captor
    lateinit var argErrorCaptorDailyForecast: ArgumentCaptor<String>

    @Captor
    lateinit var argResultCaptorHourlyForecast: ArgumentCaptor<List<ForecastResponse.Forecast>>
    @Captor
    lateinit var argErrorCaptorHourlyForecast: ArgumentCaptor<String>

    @Mock
    lateinit var useCase: ForecastUseCase

    private lateinit var viewModel: ForecastListViewModel

    private val dailyForecasts = listOf(
        DailyForecastResponse.Forecast(
            12345,
            DailyForecastResponse.Temperature(
                1.0,
                2.0
            ),
            listOf(
                DailyForecastResponse.Weather(
                    12345
                )
            )
        )
    )

    private val hourlyForecasts = listOf(
        ForecastResponse.Forecast(
            12345,
            listOf(
                ForecastResponse.Weather(
                    12345
                )
            ),
            ForecastResponse.Main(
                1.0,
                1.0,
                2.0
            )
        )
    )

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(Dispatchers.Unconfined)

        viewModel = ForecastListViewModel(useCase)
        viewModel.dailyForecastResult.observeForever(dailyForecastResult)
        viewModel.dailyForecastError.observeForever(dailyForecastError)

        viewModel.forecastResult.observeForever(hourlyForecastResult)
        viewModel.forecastError.observeForever(hourlyForecastError)
    }

    @Test
    fun `should return a response of daily forecasts data`() = runBlocking {
        val returnValue = ResultState.Success(DailyForecastResponse(dailyForecasts))
        `when`(useCase.getDailyForecast()).thenReturn(returnValue)
        viewModel.getDailyForecast()
        verify(dailyForecastResult, atLeastOnce()).onChanged(argResultCaptorDailyForecast.capture())
        assertEquals(returnValue.data.forecastList, argResultCaptorDailyForecast.allValues.first())
        clearInvocations(useCase, dailyForecastResult)
    }

    @Test
    fun `should return an error of daily forecasts data without api key`() = runBlocking {
        val returnValue = ResultState.Error("API Key Not Found")
        `when`(useCase.getDailyForecast()).thenReturn(returnValue)
        viewModel.getDailyForecast()
        verify(dailyForecastError, atLeastOnce()).onChanged(argErrorCaptorDailyForecast.capture())
        assertEquals(returnValue.error, argErrorCaptorDailyForecast.allValues.first())
        clearInvocations(useCase, dailyForecastError)
    }

    @Test
    fun `should return a response of hourly forecasts data`() = runBlocking {
        val returnValue = ResultState.Success(ForecastResponse(hourlyForecasts))
        `when`(useCase.getCurrentForecast()).thenReturn(returnValue)
        viewModel.getCurrentForecast()
        verify(
            hourlyForecastResult,
            atLeastOnce()
        ).onChanged(argResultCaptorHourlyForecast.capture())
        assertEquals(returnValue.data.forecastList, argResultCaptorHourlyForecast.allValues.first())
        clearInvocations(useCase, hourlyForecastResult)
    }

    @Test
    fun `should return an error of hourly forecasts data without api key`() = runBlocking {
        val returnValue = ResultState.Error("API Key Not Found")
        `when`(useCase.getCurrentForecast()).thenReturn(returnValue)
        viewModel.getCurrentForecast()
        verify(hourlyForecastError, atLeastOnce()).onChanged(argErrorCaptorHourlyForecast.capture())
        assertEquals(returnValue.error, argErrorCaptorHourlyForecast.allValues.first())
        clearInvocations(useCase, hourlyForecastError)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}