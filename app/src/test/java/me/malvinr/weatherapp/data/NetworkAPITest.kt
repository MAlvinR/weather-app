package me.malvinr.weatherapp.data

import junit.framework.TestCase.assertEquals
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Before
import org.junit.Test

class NetworkAPITest {

    private lateinit var mockWebServer: MockWebServer

    private lateinit var dailyForecastSuccess: MockResponse
    private lateinit var dailyForecastError: MockResponse

    private lateinit var forecastSuccess: MockResponse
    private lateinit var forecastError: MockResponse

    @Before fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.url("/")

        dailyForecastSuccess = MockResponse()
            .setBody(dailyForecastSuccessJson)
            .setResponseCode(200)

        dailyForecastError = MockResponse()
            .setBody(dailyForecastErrorJson)
            .setResponseCode(500)

        forecastSuccess = MockResponse()
            .setBody(hourlyForecastSuccessJson)
            .setResponseCode(200)

        forecastError = MockResponse()
            .setBody(hourlyForecastErrorJson)
            .setResponseCode(500)
    }

    @Test fun `should daily forecast success mock response`() {
        mockWebServer.enqueue(dailyForecastSuccess)
        assertEquals("HTTP/1.1 200 OK", dailyForecastSuccess.status)
    }

    @Test fun `should daily forecast error mock response`() {
        mockWebServer.enqueue(dailyForecastError)
        assertEquals("HTTP/1.1 500 Server Error", dailyForecastError.status)
    }

    @Test fun `should hourly forecast success mock response`() {
        mockWebServer.enqueue(forecastSuccess)
        assertEquals("HTTP/1.1 200 OK", forecastSuccess.status)
    }

    @Test fun `should hourly forecast error mock response`() {
        mockWebServer.enqueue(forecastError)
        assertEquals("HTTP/1.1 500 Server Error", forecastError.status)
    }

    @Test fun tearDown() {
        mockWebServer.shutdown()
    }
}