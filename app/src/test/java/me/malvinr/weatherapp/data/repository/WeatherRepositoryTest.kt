package me.malvinr.weatherapp.data.repository

import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import me.malvinr.weatherapp.data.entity.DailyForecastResponse
import me.malvinr.weatherapp.data.entity.ForecastResponse
import me.malvinr.weatherapp.data.service.NetworkServices
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import retrofit2.Response

class WeatherRepositoryTest {

    private var services = mock(NetworkServices::class.java)
    private lateinit var repository: WeatherRepository

    private val dailyForecasts = listOf(
        DailyForecastResponse.Forecast(
            12345,
            DailyForecastResponse.Temperature(
                1.0,
                2.0
            ),
            listOf(
                DailyForecastResponse.Weather(
                    12345
                )
            )
        )
    )

    private val hourlyForecasts = listOf(
        ForecastResponse.Forecast(
            12345,
            listOf(
                ForecastResponse.Weather(
                    12345
                )
            ),
            ForecastResponse.Main(
                1.0,
                1.0,
                2.0
            )
        )
    )

    @Before
    fun setUp() {
        repository = WeatherRepositoryImpl(services)
    }

    @Test
    fun `should get daily forecast success`() = runBlocking {
        `when`(services.getDailyForecast()).thenReturn(
            Response.success(DailyForecastResponse(dailyForecasts))
        )

        val repo = repository.getDailyForecast()

        assertEquals(repo.body(), DailyForecastResponse(dailyForecasts))
    }

    @Test
    fun `should get daily forecast error`() = runBlocking {
        `when`(services.getDailyForecast()).thenReturn(
            Response.error(401, "".toResponseBody("application/json".toMediaType()))
        )

        val repo = repository.getDailyForecast()

        assertEquals(repo.body(), null)
    }

    @Test
    fun `should get hourly forecast success`() = runBlocking {
        `when`(services.getCurrentForecast()).thenReturn(
            Response.success(ForecastResponse(hourlyForecasts))
        )

        val repo = repository.getCurrentForecast()

        assertEquals(repo.body(), ForecastResponse(hourlyForecasts))
    }

    @Test
    fun `should get hourly forecast error`() = runBlocking {
        `when`(services.getCurrentForecast()).thenReturn(
            Response.error(401, "".toResponseBody("application/json".toMediaType()))
        )

        val repo = repository.getCurrentForecast()

        assertEquals(repo.body(), null)
    }
}