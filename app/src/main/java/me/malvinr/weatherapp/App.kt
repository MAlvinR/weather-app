package me.malvinr.weatherapp

import android.app.Application
import me.malvinr.weatherapp.di.appComponent
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

open class App: Application() {
    override fun onCreate() {
        super.onCreate()
        configureKoin()
    }

    open fun configureKoin() = startKoin {
        androidLogger()
        androidContext(this@App)
        androidFileProperties()
        modules(provideComponent())
    }

    open fun provideComponent() = appComponent
}