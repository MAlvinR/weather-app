package me.malvinr.weatherapp.features

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import me.malvinr.weatherapp.data.entity.DailyForecastResponse
import me.malvinr.weatherapp.data.entity.ForecastResponse
import me.malvinr.weatherapp.databinding.ItemForecastBinding
import me.malvinr.weatherapp.databinding.ItemHeaderBinding
import me.malvinr.weatherapp.databinding.ListHourlyForecastBinding

class ForecastAdapter(private val viewModel: ForecastListViewModel) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var dailyForecasts: List<DailyForecastResponse.Forecast> = listOf()
    private var hourlyForecasts: List<ForecastResponse.Forecast> = listOf()

    private val ITEM_TODAY = 0
    private val ITEM_HOURLY_FORECAST = 1
    private val ITEM_FUTURE_DAY = 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType) {
            ITEM_TODAY -> ForecastHeaderItemViewHolder.from(parent)
            ITEM_HOURLY_FORECAST -> HourlyForecastItemViewHolder.from(parent)
            else -> ForecastItemViewHolder.from(parent)
        }
    }

    override fun getItemCount(): Int = dailyForecasts.size

    override fun getItemViewType(position: Int): Int =
        if (position == 0) ITEM_TODAY
        else if (position == 1) ITEM_HOURLY_FORECAST
        else ITEM_FUTURE_DAY


    fun setupDailyForecastList(forecasts: List<DailyForecastResponse.Forecast>?) {
        forecasts?.let {
            this.dailyForecasts = it
        }
        notifyDataSetChanged()
    }

    fun setupForecastList(forecasts: List<ForecastResponse.Forecast>) {
        this.hourlyForecasts = forecasts
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(getItemViewType(position)) {
            ITEM_TODAY -> {
                val headerHolder: ForecastHeaderItemViewHolder = holder as ForecastHeaderItemViewHolder
                headerHolder.bind(dailyForecasts[position])
            }
            ITEM_HOURLY_FORECAST -> {
                val hourlyForecastHolder: HourlyForecastItemViewHolder = holder as HourlyForecastItemViewHolder
                hourlyForecastHolder.bind(hourlyForecasts)
            }
            ITEM_FUTURE_DAY -> {
                val forecastHolder: ForecastItemViewHolder = holder as ForecastItemViewHolder
                forecastHolder.bind(dailyForecasts[position])
            }
        }
    }


    class ForecastHeaderItemViewHolder(val binding: ItemHeaderBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(forecast: DailyForecastResponse.Forecast) {
            binding.viewmodel = HeaderViewModel(binding.root.context, forecast)
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ForecastHeaderItemViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemHeaderBinding.inflate(layoutInflater, parent, false)

                return ForecastHeaderItemViewHolder(binding)
            }
        }
    }

    class HourlyForecastItemViewHolder(val binding: ListHourlyForecastBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(forecasts: List<ForecastResponse.Forecast>) {
//            binding.viewmodel = HeaderViewModel(binding.root.context, forecast)
//            binding.executePendingBindings()
            val adapter = HourlyForecastAdapter()
            binding.listHourlyForecast.adapter = adapter
            adapter.setupList(forecasts)
        }

        companion object {
            fun from(parent: ViewGroup): HourlyForecastItemViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListHourlyForecastBinding.inflate(layoutInflater, parent, false)

                return HourlyForecastItemViewHolder(binding)
            }
        }
    }

    class ForecastItemViewHolder(val binding: ItemForecastBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(forecast: DailyForecastResponse.Forecast) {
            binding.viewmodel = DailyForecastViewModel(binding.root.context, forecast)
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ForecastItemViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemForecastBinding.inflate(layoutInflater, parent, false)

                return ForecastItemViewHolder(binding)
            }
        }
    }
}