package me.malvinr.weatherapp.features

import android.util.Log
import me.malvinr.weatherapp.data.entity.DailyForecastResponse
import me.malvinr.weatherapp.data.entity.ForecastResponse
import me.malvinr.weatherapp.data.repository.WeatherRepository
import me.malvinr.weatherapp.util.ext.fetchState
import me.malvinr.weatherapp.util.state.ResultState

class ForecastUseCase(private val repository: WeatherRepository) {

    suspend fun getCurrentForecast(): ResultState<ForecastResponse> = fetchState {
        val response = repository.getCurrentForecast()
        if (response.isSuccessful) {
            ResultState.Success(response.body()!!)
        } else {
            ResultState.Error("Something Error!")
        }
    }

    suspend fun getDailyForecast(): ResultState<DailyForecastResponse> = fetchState {
        val response = repository.getDailyForecast()
        if (response.isSuccessful) {
            ResultState.Success(response.body()!!)
        } else {
            ResultState.Error("Something Error!")
        }
    }
}