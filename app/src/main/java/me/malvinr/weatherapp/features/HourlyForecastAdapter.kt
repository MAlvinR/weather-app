package me.malvinr.weatherapp.features

import android.app.Activity
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_hourly_forecast.view.*
import me.malvinr.weatherapp.data.entity.ForecastResponse
import me.malvinr.weatherapp.databinding.ItemHourlyForecastBinding

class HourlyForecastAdapter : RecyclerView.Adapter<HourlyForecastAdapter.HourlyForecastItemViewHolder>() {

    private var forecasts: List<ForecastResponse.Forecast> = listOf()

    class HourlyForecastItemViewHolder(val binding: ItemHourlyForecastBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(forecast: ForecastResponse.Forecast) {
            binding.viewmodel = HourlyForecastViewModel(binding.root.context, forecast)
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): HourlyForecastItemViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemHourlyForecastBinding.inflate(layoutInflater, parent, false)

                val displayMetrics = DisplayMetrics()
                (parent.context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)

                val width = displayMetrics.widthPixels / 5

                binding.root.hourly_forecast_container.layoutParams.width = width

                return HourlyForecastItemViewHolder(binding)
            }
        }
    }

    fun setupList(forecasts: List<ForecastResponse.Forecast>) {
        this.forecasts = forecasts
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): HourlyForecastItemViewHolder {
        return HourlyForecastItemViewHolder.from(parent)
    }

    override fun getItemCount(): Int = forecasts.size

    override fun onBindViewHolder(holder: HourlyForecastItemViewHolder, position: Int) {
        holder.bind(forecasts[position])
    }
}