package me.malvinr.weatherapp.features

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import me.malvinr.weatherapp.data.entity.DailyForecastResponse
import me.malvinr.weatherapp.util.DateUtils
import me.malvinr.weatherapp.util.WeatherUtils

class DailyForecastViewModel(context: Context, dailyForecast: DailyForecastResponse.Forecast) {
    val formattedTemperature: String = WeatherUtils.formatTemperature(context, dailyForecast.temp.maxTemp)
    val weatherIcon: Drawable = ContextCompat.getDrawable(context, WeatherUtils.getGreyArtResourceIdForWeatherCondition(dailyForecast.weather[0].id))!!
    val dayName = DateUtils.getDayName(dailyForecast.datetime)
}