package me.malvinr.weatherapp.features

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import me.malvinr.weatherapp.data.entity.DailyForecastResponse
import me.malvinr.weatherapp.util.DateUtils
import me.malvinr.weatherapp.util.WeatherUtils

class HeaderViewModel(context: Context, dailyForecast: DailyForecastResponse.Forecast) {
    val formattedTemperature: String = WeatherUtils.formatTemperature(context, dailyForecast.temp.maxTemp)
    val weatherIcon: Drawable = ContextCompat.getDrawable(context, WeatherUtils.getWhiteArtResourceIdForWeatherCondition(dailyForecast.weather[0].id))!!
    val weatherDescription: String = WeatherUtils.getWeatherDescription(context, dailyForecast.weather[0].id)
    val dateNow = DateUtils.getFriendlyDateString(dailyForecast.datetime)
}