package me.malvinr.weatherapp.features

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import me.malvinr.weatherapp.data.entity.ForecastResponse
import me.malvinr.weatherapp.util.DateUtils
import me.malvinr.weatherapp.util.WeatherUtils

class HourlyForecastViewModel(context: Context, dailyForecast: ForecastResponse.Forecast) {
    val formattedTemperature: String = WeatherUtils.formatTemperature(context, dailyForecast.main.temp)
    val weatherIcon: Drawable = ContextCompat.getDrawable(context, WeatherUtils.getGreyArtResourceIdForWeatherCondition(dailyForecast.weather[0].id))!!
    val hour = DateUtils.getHour(dailyForecast.datetime)
}