package me.malvinr.weatherapp.features

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import me.malvinr.weatherapp.data.entity.DailyForecastResponse
import me.malvinr.weatherapp.data.entity.ForecastResponse
import me.malvinr.weatherapp.util.state.LoaderState
import me.malvinr.weatherapp.util.state.ResultState

interface ForecastListContract {
    fun getCurrentForecast()
    fun getDailyForecast()
}

class ForecastListViewModel(
    private val useCase: ForecastUseCase
): ViewModel(), ForecastListContract {

    private val _state = MutableLiveData<LoaderState>()

    val state: LiveData<LoaderState>
        get() = _state

    private val _forecastResult = MutableLiveData<List<ForecastResponse.Forecast>>()
    val forecastResult: LiveData<List<ForecastResponse.Forecast>>
        get() = _forecastResult

    private val _forecastError = MutableLiveData<String>()
    val forecastError: LiveData<String>
        get() = _forecastError

    private val _dailyForecastResult = MutableLiveData<List<DailyForecastResponse.Forecast>>()
    val dailyForecastResult: LiveData<List<DailyForecastResponse.Forecast>>
        get() = _dailyForecastResult

    private val _dailyForecastError = MutableLiveData<String>()
    val dailyForecastError: LiveData<String>
        get() = _dailyForecastError

    init {
        getCurrentForecast()
        getDailyForecast()
    }

    override fun getCurrentForecast() {
        _state.value = LoaderState.ShowLoading
        viewModelScope.launch {
            val result = useCase.getCurrentForecast()
            withContext(Dispatchers.Main) {
//                _state.value = LoaderState.HideLoading
                when (result) {
                    is ResultState.Success -> _forecastResult.value = result.data.forecastList
                    is ResultState.Error -> _forecastError.value = result.error
                }
            }
        }
    }

    override fun getDailyForecast() {
//        _state.value = LoaderState.ShowLoading
        viewModelScope.launch {
            val result = useCase.getDailyForecast()
            withContext(Dispatchers.Main) {
                _state.value = LoaderState.HideLoading
                when (result) {
                    is ResultState.Success -> _dailyForecastResult.value = result.data.forecastList
                    is ResultState.Error -> _dailyForecastError.value = result.error
                }
            }
        }
    }
}