package me.malvinr.weatherapp.features

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import me.malvinr.weatherapp.R
import me.malvinr.weatherapp.databinding.ActivityForecastBinding
import me.malvinr.weatherapp.util.state.LoaderState
import org.koin.androidx.viewmodel.ext.android.viewModel

class ForecastActivity : AppCompatActivity() {

    private val viewModel: ForecastListViewModel by viewModel()
    private lateinit var viewDataBinding: ActivityForecastBinding
    private lateinit var forecastAdapter: ForecastAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = DataBindingUtil.setContentView<ActivityForecastBinding>(
            this,
            R.layout.activity_forecast
        ).apply {
            viewmodel = viewModel
            lifecycleOwner = this@ForecastActivity
        }
//        viewModel.getCurrentForecast()
//        viewModel.getDailyForecast()
        initObserver()
        setupListAdapter()
    }

    private fun initObserver() {
        viewModel.state.observe(this, Observer { state ->
            when (state) {
                is LoaderState.ShowLoading -> Log.d("FORECASTLOADERSTATE", "loading")
                is LoaderState.HideLoading -> Log.d("FORECASTLOADERSTATE", "complete")
            }
        })

        viewModel.forecastResult.observe(this, Observer { forecastResponse ->
            forecastAdapter.setupForecastList(forecastResponse)
            Log.d("FORECASTRESPONSE", forecastResponse.toString())
        })

        viewModel.dailyForecastResult.observe(this, Observer { dailyForecastResponse ->
            Log.d("FORECASTRESPONSEDAILY", dailyForecastResponse.toString())
        })

    }

    private fun setupListAdapter() {
        val viewModel = viewDataBinding.viewmodel
        if (viewModel != null) {
            forecastAdapter = ForecastAdapter(viewModel)
            viewDataBinding.listForecast.adapter = forecastAdapter
        } else {
            Log.d("FORECASTADAPTER", "ViewModel is not initialized when attempting set up adapter.")
        }
    }
}
