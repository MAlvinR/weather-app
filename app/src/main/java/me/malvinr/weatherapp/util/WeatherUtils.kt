package me.malvinr.weatherapp.util

import android.content.Context
import android.util.Log
import androidx.core.content.ContextCompat
import me.malvinr.weatherapp.R

object WeatherUtils {

    fun formatTemperature(context: Context, temperature: Double): String {
        val roundedTemp = Math.round(temperature)
        val temperatureFormatResourceId = R.string.format_temperature
        return String.format(context.getString(temperatureFormatResourceId), roundedTemp)
    }

    fun getGreyArtResourceIdForWeatherCondition(weatherId: Int): Int {
        if (weatherId in 200..232) {
            return R.drawable.ic_cloud_lightning
        } else if (weatherId in 300..321) {
            return R.drawable.ic_cloud_drizzle
        } else if (weatherId in 500..504) {
            return R.drawable.ic_cloud_rain
        } else if (weatherId in 520..531) {
            return R.drawable.ic_cloud_rain
        } else if (weatherId == 761 || weatherId == 771 || weatherId == 781) {
            return R.drawable.ic_cloud_lightning
        } else if (weatherId == 800) {
            return R.drawable.ic_sun
        } else if (weatherId in 801..804) {
            return R.drawable.ic_cloud_cloudy
        } else if (weatherId in 900..906) {
            return R.drawable.ic_cloud_lightning
        } else if (weatherId in 951..957) {
            return R.drawable.ic_sun
        } else if (weatherId in 958..962) {
            return R.drawable.ic_cloud_lightning
        }

        Log.e("WEATHERICON", "Unknown Weather: $weatherId")
        return R.drawable.ic_cloud_cloudy
    }

    fun getWhiteArtResourceIdForWeatherCondition(weatherId: Int): Int {
        if (weatherId in 200..232) {
            return R.drawable.ic_cloud_lightning_white
        } else if (weatherId in 300..321) {
            return R.drawable.ic_cloud_drizzle_white
        } else if (weatherId in 500..504) {
            return R.drawable.ic_cloud_rain_white
        } else if (weatherId in 520..531) {
            return R.drawable.ic_cloud_rain_white
        } else if (weatherId == 761 || weatherId == 771 || weatherId == 781) {
            return R.drawable.ic_cloud_lightning_white
        } else if (weatherId == 800) {
            return R.drawable.ic_sun
        } else if (weatherId in 801..804) {
            return R.drawable.ic_cloud_cloudy_white
        } else if (weatherId in 900..906) {
            return R.drawable.ic_cloud_lightning_white
        } else if (weatherId in 951..957) {
            return R.drawable.ic_sun
        } else if (weatherId in 958..962) {
            return R.drawable.ic_cloud_lightning_white
        }

        Log.e("WEATHERICON", "Unknown Weather: $weatherId")
        return R.drawable.ic_cloud_cloudy_white
    }

    fun getWeatherDescription(context: Context, weatherId: Int): String {
        val weatherImageId = getGreyArtResourceIdForWeatherCondition(weatherId)
        val weatherIcon = ContextCompat.getDrawable(context, weatherImageId)

        return when (weatherImageId) {
            R.drawable.ic_sun -> "Cerah"
            R.drawable.ic_cloud_cloudy -> "Berawan"
            R.drawable.ic_cloud_rain -> "Hujan"
            R.drawable.ic_cloud_lightning -> "Petir"
            R.drawable.ic_cloud_drizzle -> "Gerimis"
            else -> "Deskripsi Cuaca"
        }
    }
}