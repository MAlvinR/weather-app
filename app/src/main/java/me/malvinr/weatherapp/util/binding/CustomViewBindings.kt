package me.malvinr.weatherapp.util.binding

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import me.malvinr.weatherapp.data.entity.DailyForecastResponse
import me.malvinr.weatherapp.features.ForecastAdapter

@BindingAdapter("app:items")
fun setItems(recyclerView: RecyclerView, items: List<DailyForecastResponse.Forecast>?) {
    (recyclerView.adapter as ForecastAdapter).setupDailyForecastList(items)
}