package me.malvinr.weatherapp.util

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    private fun normalizeDate(date: Long): Date = Date(date * 1000)

    private val timezone = TimeZone.getTimeZone("Asia/Jakarta")

    private val localeLanguage = Locale("in")

    fun getFriendlyDateString(epochMilliSeconds: Long): String {
        val date = normalizeDate(epochMilliSeconds)
        val dateFormatter = SimpleDateFormat("EEEE, d MMMM yyyy", localeLanguage)
        dateFormatter.timeZone = timezone

        return dateFormatter.format(date)
    }

    fun getDayName(epochMilliSeconds: Long): String {
        val date = normalizeDate(epochMilliSeconds)
        val dateFormatter = SimpleDateFormat("EEEE", localeLanguage)
        dateFormatter.timeZone = timezone

        return dateFormatter.format(date)
    }

    fun getHour(epochMilliSeconds: Long): String {
        val date = normalizeDate(epochMilliSeconds)
        val dateFormatter = SimpleDateFormat("ha", localeLanguage)
        dateFormatter.timeZone = timezone

        return dateFormatter.format(date)
    }
}