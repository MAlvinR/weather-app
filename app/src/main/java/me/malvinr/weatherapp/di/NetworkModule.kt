package me.malvinr.weatherapp.di

import me.malvinr.weatherapp.data.network.Network
import me.malvinr.weatherapp.data.service.NetworkServices
import org.koin.dsl.module

val networkModule = module {

    single { Network.networkClient().create(NetworkServices::class.java) }
}