package me.malvinr.weatherapp.di

import me.malvinr.weatherapp.data.repository.WeatherRepository
import me.malvinr.weatherapp.data.repository.WeatherRepositoryImpl
import me.malvinr.weatherapp.features.ForecastUseCase
import me.malvinr.weatherapp.features.ForecastListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val featureWeatherModule = module {

    factory { WeatherRepositoryImpl(get()) as WeatherRepository }
    factory { ForecastUseCase(get()) }
    viewModel { ForecastListViewModel(get()) }
}