package me.malvinr.weatherapp.di

val appComponent = listOf(
    networkModule,
    featureWeatherModule
)