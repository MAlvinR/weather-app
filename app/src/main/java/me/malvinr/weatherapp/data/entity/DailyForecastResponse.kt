package me.malvinr.weatherapp.data.entity

import com.google.gson.annotations.SerializedName

data class DailyForecastResponse(
    @SerializedName("list") val forecastList: List<Forecast>
) {
    data class Forecast(
        @SerializedName("dt") val datetime: Long,
        @SerializedName("temp") val temp: Temperature,
        @SerializedName("weather") val weather: List<Weather>
    )

    data class Temperature(
        @SerializedName("min") val minTemp: Double,
        @SerializedName("max") val maxTemp: Double
    )

    data class Weather(
        @SerializedName("id") val id: Int
    )
}