package me.malvinr.weatherapp.data.repository

import me.malvinr.weatherapp.data.entity.DailyForecastResponse
import me.malvinr.weatherapp.data.entity.ForecastResponse
import retrofit2.Response

interface WeatherRepository {
    suspend fun getCurrentForecast(): Response<ForecastResponse>
    suspend fun getDailyForecast(): Response<DailyForecastResponse>
}