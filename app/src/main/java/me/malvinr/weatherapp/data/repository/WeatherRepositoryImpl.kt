package me.malvinr.weatherapp.data.repository

import me.malvinr.weatherapp.data.entity.DailyForecastResponse
import me.malvinr.weatherapp.data.entity.ForecastResponse
import me.malvinr.weatherapp.data.service.NetworkServices
import retrofit2.Response

class WeatherRepositoryImpl constructor(
    private val service: NetworkServices
) : WeatherRepository {

    override suspend fun getCurrentForecast(): Response<ForecastResponse> {
        return service.getCurrentForecast()
    }

    override suspend fun getDailyForecast(): Response<DailyForecastResponse> {
        return service.getDailyForecast()
    }
}