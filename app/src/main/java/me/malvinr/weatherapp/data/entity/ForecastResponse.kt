package me.malvinr.weatherapp.data.entity

import com.google.gson.annotations.SerializedName

data class ForecastResponse(
    @SerializedName("list") val forecastList: List<Forecast>
) {
    data class Forecast(
        @SerializedName("dt") val datetime: Long,
        @SerializedName("weather") val weather: List<Weather>,
        @SerializedName("main") val main: Main
    )

    data class Main(
        @SerializedName("temp") val temp: Double,
        @SerializedName("temp_min") val minTemp: Double,
        @SerializedName("temp_max") val maxTemp: Double
    )

    data class Weather(
        @SerializedName("id") val id: Int
    )
}