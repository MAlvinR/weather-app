package me.malvinr.weatherapp.data.service

import me.malvinr.weatherapp.data.entity.DailyForecastResponse
import me.malvinr.weatherapp.data.entity.ForecastResponse
import retrofit2.Response
import retrofit2.http.GET

interface NetworkServices {

    /* Get Forecast data every everyday within 7 days */
    @GET("forecast/daily")
    suspend fun getDailyForecast(): Response<DailyForecastResponse>

    /* Get Forecast data every 3 hours */
    @GET("forecast?cnt=5")
    suspend fun getCurrentForecast(): Response<ForecastResponse>
}