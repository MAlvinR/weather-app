package me.malvinr.weatherapp.data.network

import me.malvinr.weatherapp.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class NetworkInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val url = request.url
            .newBuilder()
            .addQueryParameter("q", "Jakarta")
            .addQueryParameter("appid", BuildConfig.APP_ID)
            .addQueryParameter("units", "metric")
            .build()
        request = request.newBuilder().url(url).build()
        return chain.proceed(request)
    }
}